set nocp
syntax on
set ts=4
set shiftwidth=4
set expandtab
set clipboard=unnamed
set mouse=c
set nu
set cursorline
set cursorcolumn
set colorcolumn=80
colorscheme slate

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Jenkinsfile
if has("autocmd")
  autocmd BufRead,BufNewFile Jenkinsfile set syntax=groovy
endif

